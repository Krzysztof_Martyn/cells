﻿using System.Collections.Generic;
using UnityEngine;

public class GenerateCells : MonoBehaviour
{
    public bool isQuadTree = false;
    public int QuadTreeDepth = 4;
    public int CellCount = 100;
    public Mesh mesh;
    public UnityEngine.Material material;
    private List<Cell> cells;
    private IElementStructure hashTerrain;

    private void Start()
    {
        QuadTreeDepth = QuadTreeDepth < 0 ? 0 : QuadTreeDepth;
        CellCount = CellCount < 0 ? 0 : CellCount;

        if (isQuadTree)
            hashTerrain = new QuadTreeNode(null, new Vector2(100.0f, 100.0f), 0, new Vector4(0.0f, 0.0f, 200.0f, 200.0f));
        else
            hashTerrain = new HashTerrain();
        cells = new List<Cell>();
        for (int i = 0; i < CellCount; i++)
        {
            Cell cell = new Cell(i.ToString(), new Vector3(Random.value * 200, 0.5f, Random.value * 200));
            cell.mesh = mesh;
            cell.material = material;
            hashTerrain.addElement(cell);
            cells.Add(cell);
        }
        if (hashTerrain.GetType() == typeof(QuadTreeNode))
        {
            for (int i = 0; i < QuadTreeDepth; i++)
                ((QuadTreeNode)hashTerrain).checkSize();

            ((QuadTreeNode)hashTerrain).NodeNeighbor(1);
            Debug.Log("printTree\n\r" + ((QuadTreeNode)hashTerrain).printTree(0));
        }
    }

    private void Update()
    {
        RaycastHit HitInfo;
        foreach (Cell cell in cells)
        {
            float speed = 1.0f;
            float alpha = 0.1f;
            Vector3 move = cell.move;

            if (cell.moveCounter == 0)
            {
                move = new Vector3(0, move.y + Random.value * 50.0f - 25.0f, 0);
                Cell cell2 = hashTerrain.NearestNeighbor(cell);
                cell.moveCounter = (int)Random.Range(1, 20);
                if (cell2 != null)
                {
                    cell._cell.transform.LookAt(cell2._cell.transform);
                    Vector3 lookVec = cell2._cell.transform.position - cell._cell.transform.position;
                    if (lookVec != Vector3.zero)
                    {
                        var targetRotation = Quaternion.LookRotation(lookVec);
                        targetRotation.y = targetRotation.y * alpha + cell._cell.transform.rotation.y * (1 - alpha);
                        cell._cell.transform.rotation = targetRotation;
                        cell._cell.transform.Rotate(new Vector3(0, 1, 0), move.y);
                    }
                }
            }
            cell.moveCounter--;

            cell.move = move;
            move.x *= Time.deltaTime;
            move.z *= Time.deltaTime;

            hashTerrain.removeElement(cell);

            Vector3 FORWARD = cell._cell.transform.TransformDirection(Vector3.forward);
            FORWARD.x *= speed;
            FORWARD.z *= speed;

            cell._cell.transform.localPosition += FORWARD;
            cell._cell.transform.position = new Vector3(Mathf.Min(Mathf.Max(0, cell.position.x), 200), 0.5f, Mathf.Min(Mathf.Max(0, cell.position.z), 200));

            Debug.DrawRay(cell._cell.transform.position, cell._cell.transform.TransformDirection(Vector3.forward), Color.yellow);
            if (Physics.Raycast(cell._cell.transform.position, cell._cell.transform.TransformDirection(Vector3.forward), out HitInfo, 10))
            {
                //Debug.Log(cell._cell.name + " There is something in front of the object! " + HitInfo.collider.gameObject.name );
                cell.cellComponent.Hp++;
                HitInfo.collider.gameObject.GetComponent<CellComponent>().Hp--;
            }
            hashTerrain.addElement(cell);
        }
    }

    private void Update2()
    {
        foreach (Cell cell in cells)
        {
            float speed = 1.0f;
            Vector3 move = cell.move;
            if (cell.moveCounter == 0)
            {
                move = new Vector3(move.x + Random.value / 100 - 0.005f, 0, move.z + Random.value / 100 - 0.005f);
                Cell cell2 = hashTerrain.NearestNeighbor(cell);
                if (cell2 != null)
                {
                    var vec = cell2.position - cell.position;
                    vec.Normalize();
                    move.x = move.x + vec.x * 0.1f;
                    move.z = move.z + vec.z * 0.1f;
                }
                move.x = Mathf.Min(1.0f, move.x);
                move.z = Mathf.Min(1.0f, move.z);
                cell.moveCounter = (int)Random.Range(1, 20);
            }
            cell.moveCounter--;
            if (cell.position.x >= 200)
                move.x = move.x > 0 ? -move.x : move.x;
            if (cell.position.x < 0)
                move.x = move.x > 0 ? move.x : -move.x;

            if (cell.position.z >= 200)
                move.z = move.z > 0 ? -move.z : move.z;
            if (cell.position.z < 0)
                move.z = move.z > 0 ? move.z : -move.z;

            move.x *= Time.deltaTime;
            move.z *= Time.deltaTime;
            cell.move = move;

            hashTerrain.removeElement(cell);
            cell._cell.transform.Translate(move, Space.World);
            cell._cell.transform.Rotate(move);
            Vector3 FORWARD = cell._cell.transform.TransformDirection(Vector3.forward);
            FORWARD.x *= speed;
            FORWARD.z *= speed;

            cell._cell.transform.localPosition += FORWARD;
            hashTerrain.addElement(cell);
        }
    }
}