﻿using System.Collections.Generic;
using UnityEngine;

public class HashTerrain : IElementStructure
{
    private Dictionary<int, List<Cell>> data = new Dictionary<int, List<Cell>>();
    private float BucketSize = 10.0f;
    public Vector4 Size = new Vector4(0.0f, 0.0f, 200.0f, 200.0f);
    public int MaxX = 200;

    public void addElement(Cell elem)
    {
        // Debug.Log("addElement  ");
        if (data.ContainsKey((int)(elem.position.x / BucketSize) + (int)(elem.position.z / BucketSize) * MaxX))
        {
            //Debug.Log("addElement 1 ");
            data[(int)(elem.position.x / BucketSize) + (int)(elem.position.z / BucketSize) * MaxX].Add(elem);
        }
        else
        {
            //Debug.Log("addElement 2 ");
            data.Add((int)(elem.position.x / BucketSize) + (int)(elem.position.z / BucketSize) * MaxX, new List<Cell>());
            data[(int)(elem.position.x / BucketSize) + (int)(elem.position.z / BucketSize) * MaxX].Add(elem);
        }
    }

    public void removeElement(Cell elem)
    {
        //Debug.Log("removeElement  ");
        List<Cell> elements = data[(int)(elem.position.x / BucketSize) + (int)(elem.position.z / BucketSize) * MaxX];
        elements.Remove(elem);
    }

    public Cell NearestNeighbor(Cell elem, int base_Node = -1)
    {
        float distance = float.MaxValue;
        Cell candidate = null;
        for (int x = -1; x < 2; x++)
        {
            if (elem.position.x - x >= Size[0] && elem.position.x + x < Size[2])
            {
                for (int y = -1; y < 2; y++)
                {
                    if (elem.position.z - y >= Size[1] && elem.position.z + y < Size[3])
                        if (data.ContainsKey((int)(elem.position.x / BucketSize + x) + (int)(elem.position.z / BucketSize + y) * MaxX))
                        {
                            List<Cell> elements = data[(int)(elem.position.x / BucketSize + x) + (int)(elem.position.z / BucketSize + y) * MaxX];
                            foreach (Cell e in elements)
                            {
                                if (e.name != elem.name)
                                {
                                    elem.Distance(e, ref distance, ref candidate);
                                }
                            }
                        }
                }
            }
        }
        return candidate;
    }
}