﻿using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public float dragSpeed = 2;
    public int cameraCurrentZoom = 8;
    public int cameraZoomMax = 20;
    public int cameraZoomMin = 5;
    private Vector3 dragOrigin;

    private void Start()
    {
        Camera.main.orthographic = true;
        Camera.main.orthographicSize = cameraCurrentZoom;
    }

    private void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
        {
            if (cameraCurrentZoom < cameraZoomMax)
            {
                cameraCurrentZoom += 1;
                Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize + 1);
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            if (cameraCurrentZoom > cameraZoomMin)
            {
                cameraCurrentZoom -= 1;
                Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize - 1);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }

        if (!Input.GetMouseButton(0)) return;

        Vector3 pos = Camera.main.ScreenToViewportPoint(dragOrigin - Input.mousePosition);
        Vector3 move = new Vector3(pos.x * dragSpeed, 0, pos.y * dragSpeed);

        transform.Translate(move, Space.World);
    }
}