﻿public interface IElementStructure
{
    void addElement(Cell elem);

    void removeElement(Cell elem);

    Cell NearestNeighbor(Cell elem, int base_Node = -1);
}