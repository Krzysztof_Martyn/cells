﻿using System.Collections.Generic;
using UnityEngine;

public class QuadTreeNode : IElementStructure
{
    public bool isLeaf { get; private set; }
    public QuadTreeNode[] children;
    private List<Cell> _elements;
    private QuadTreeNode _parrent;

    public List<Cell> elements
    {
        get
        {
            if (isLeaf)
                return _elements;
            else
            {
                List<Cell> e = new List<Cell>();
                e.AddRange(children[0].elements);
                e.AddRange(children[1].elements);
                e.AddRange(children[2].elements);
                e.AddRange(children[3].elements);
                return e;
            }
        }
    }

    public Vector2 center { get; private set; }
    public Vector4 square { get; private set; }
    public int depth { get; private set; }
    public int childrenCount { get; private set; }
    private const int maxElem = 10;
    private const int minElem = 2;
    public QuadTreeNode LeftNode, RightNode, TopNode, BottomNode;

    public QuadTreeNode(QuadTreeNode parrent, Vector2 _center, int _depth, Vector4 _square)
    {
        _parrent = parrent;
        isLeaf = true;
        _elements = new List<Cell>();
        depth = _depth;
        square = _square;
        childrenCount = 0;
        center = _center;
    }

    public void addElement(Cell elem)
    {
        childrenCount++;
        if (isLeaf)
        {
            _elements.Add(elem);
        }
        else
        {
            int id = 0;
            if (elem.position.x >= center.x)
                id += 1;
            if (elem.position.z >= center.y)
                id += 2;
            children[id].addElement(elem);
        }
    }

    public void removeElement(Cell elem)
    {
        childrenCount--;
        if (isLeaf)
        {
            _elements.Remove(elem);
        }
        else
        {
            int id = 0;
            if (elem.position.x >= center.x)
                id += 1;
            if (elem.position.z >= center.y)
                id += 2;
            children[id].removeElement(elem);
        }
    }

    public string printTree(int depth)
    {
        if (isLeaf)
        {
            return new string(' ', depth) + depth.ToString() + " " + _elements.Count.ToString();
        }
        else
        {
            string res = new string(' ', depth) + depth.ToString();

            res += "\n\r" + children[0].printTree(depth + 1);
            res += "\n\r" + children[1].printTree(depth + 1);
            res += "\n\r" + children[2].printTree(depth + 1);
            res += "\n\r" + children[3].printTree(depth + 1);
            return res;
        }
    }

    public void NodeNeighbor(int d)

    {
        if (!isLeaf)
        {
            if (d == depth)
            {
                if (RightNode != null)
                {
                    children[1].RightNode = RightNode.children[0];
                    RightNode.children[0].LeftNode = children[1];

                    children[3].RightNode = RightNode.children[2];
                    RightNode.children[2].LeftNode = children[3];
                }
                if (TopNode != null)
                {
                    children[2].TopNode = TopNode.children[0];
                    TopNode.children[0].BottomNode = children[2];

                    children[3].TopNode = TopNode.children[1];
                    TopNode.children[1].BottomNode = children[3];
                }
            }
            else
            {
                children[0].NodeNeighbor(d);
                children[1].NodeNeighbor(d);
                children[2].NodeNeighbor(d);
                children[3].NodeNeighbor(d);
            }
        }
    }

    public void checkSize()
    {
        if (isLeaf && _elements.Count > maxElem)
        {
            children = new QuadTreeNode[4];
            children[0] = new QuadTreeNode(this, new Vector2((square[0] + center[0]) / 2.0f, (square[1] + center[1]) / 2.0f), depth + 1, new Vector4(square[0], square[1], center[0], center[1]));
            children[1] = new QuadTreeNode(this, new Vector2((square[2] + center[0]) / 2, (square[1] + center[1]) / 2), depth + 1, new Vector4(square[2], square[1], center[0], center[1]));
            children[2] = new QuadTreeNode(this, new Vector2((square[0] + center[0]) / 2, (square[3] + center[1]) / 2), depth + 1, new Vector4(square[0], square[3], center[0], center[1]));
            children[3] = new QuadTreeNode(this, new Vector2((square[2] + center[0]) / 2, (square[3] + center[1]) / 2), depth + 1, new Vector4(square[2], square[3], center[0], center[1]));

            children[0].RightNode = children[1];
            children[1].LeftNode = children[0];
            children[2].RightNode = children[3];
            children[3].LeftNode = children[2];

            children[0].TopNode = children[2];
            children[2].BottomNode = children[0];
            children[1].TopNode = children[3];
            children[3].BottomNode = children[1];

            isLeaf = false;
            foreach (Cell elem in _elements)
            {
                addElement(elem);
            }
            _elements = null;
        }
        else if (!isLeaf && childrenCount < minElem)
        {
            _elements = elements;
            for (int i = 0; i < 4; i++)
                children[i].Dispose();
            children = null;
            isLeaf = true;
        }
        else if (!isLeaf)
        {
            for (int i = 0; i < 4; i++)
                children[i].checkSize();
        }
    }

    public void Dispose()
    {
        if (isLeaf)
        {
            _elements = null;
        }
        else
        {
            for (int i = 0; i < 4; i++)
                children[i].Dispose();
            children = null;
        }
    }

    public Cell NearestNeighbor(Cell elem, int base_Node = 0)
    {
        float distance = float.MaxValue;
        Cell candidate = null;

        if (isLeaf && elements.Count > 0)
        {
            foreach (Cell e in elements)
            {
                if (e.name != elem.name)
                {
                    elem.Distance(e, ref distance, ref candidate);
                }
            }

            if (LeftNode != null && elem.position.x - distance < square[0] && base_Node % 2 == 0)
            {
                elem.Distance(LeftNode.NearestNeighbor(elem, base_Node + 1), ref distance, ref candidate);
            }
            if (RightNode != null && elem.position.x + distance > square[2] && base_Node % 2 == 0)
            {
                elem.Distance(RightNode.NearestNeighbor(elem, base_Node + 1), ref distance, ref candidate);
            }
            if (TopNode != null && elem.position.y - distance < square[1] && base_Node < 2)
            {
                elem.Distance(TopNode.NearestNeighbor(elem, base_Node + 2), ref distance, ref candidate);
            }
            if (BottomNode != null && elem.position.y + distance > square[3] && base_Node < 2)
            {
                elem.Distance(BottomNode.NearestNeighbor(elem, base_Node + 2), ref distance, ref candidate);
            }
        }
        else if (!isLeaf)
        {
            int id = 0;
            if (elem.position.x >= center.x)
                id += 1;
            if (elem.position.z >= center.y)
                id += 2;
            candidate = children[id].NearestNeighbor(elem);

            if (candidate == null)
            {
            }
        }
        //return null;
        return candidate;
    }
}