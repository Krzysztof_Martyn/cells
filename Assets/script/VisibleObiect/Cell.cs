﻿using UnityEngine;

public class Cell : VisibleObject
{
    public GameObject _cell
    {
        get { return _obiect; }
        set { _obiect = value; }
    }

    private BoxCollider _collider;
    public CellComponent cellComponent;
    public Vector3 move { get; set; }
    public int moveCounter = 0;

    public Cell() : base()
    {
        move = new Vector3(0, 0, 0);

        _collider = _cell.AddComponent<BoxCollider>() as BoxCollider;
        cellComponent = _cell.AddComponent<CellComponent>() as CellComponent;
        cellComponent.cell = this;
        cellComponent.Hp = 100;
    }

    public Cell(string name) : this()
    {
        _cell.name = name;
        _cell.transform.position = new Vector3(180.0f, 0.5f, 180.0f);
    }

    public Cell(string name, Vector3 position) : base(name, position)
    {
        _collider = _cell.AddComponent<BoxCollider>() as BoxCollider;
        cellComponent = _cell.AddComponent<CellComponent>() as CellComponent;
        cellComponent.cell = this;
        cellComponent.Hp = 100;
    }

    public void Distance(Cell elem_other, ref float distance, ref Cell nearest)
    {
        if (elem_other != null)
        {
            float dist = Vector3.Distance(this.position, elem_other.position);
            if (dist < distance)
            {
                distance = dist;
                nearest = elem_other;
            }
        }
    }
}