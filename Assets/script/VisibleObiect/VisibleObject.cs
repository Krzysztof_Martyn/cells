﻿using UnityEngine;

public class VisibleObject
{
    protected GameObject _obiect;
    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;

    public Mesh mesh
    {
        set { _meshFilter.mesh = value; }
        get { return _meshFilter.mesh; }
    }

    public UnityEngine.Material material
    {
        set { _meshRenderer.material = value; }
    }

    public Vector3 position
    {
        get
        {
            return _obiect.transform.position;
        }
    }

    public string name
    {
        get
        {
            return _obiect.name;
        }
        set
        {
            _obiect.name = value;
        }
    }

    protected VisibleObject()
    {
        _obiect = new GameObject();
        _meshRenderer = _obiect.AddComponent<MeshRenderer>() as MeshRenderer;
        _meshRenderer.enabled = true;
        _meshFilter = _obiect.AddComponent<MeshFilter>() as MeshFilter;
    }

    public VisibleObject(string _name) : this()
    {
        name = _name;
    }

    public VisibleObject(string name, Vector3 position) : this()
    {
        _obiect.name = name;
        _obiect.transform.position = position;
    }
}